package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class FinanceReport {
    private Payment[] array;
    private String NameOfCompiler;
    private int DayOfCreation, MonthOfCreation, YearOfCreation;

    FinanceReport(int DayOfCreation, int MountOfCreation, int YearOfCreation, String NameOfCompiler, int size) {
        array = new Payment[size];
        this.DayOfCreation = DayOfCreation;
        this.MonthOfCreation = MountOfCreation;
        this.YearOfCreation = YearOfCreation;
        this.NameOfCompiler = NameOfCompiler;
        for(int i=0;i<size;i++)
        {
            try {
                FinanceReportAdd(i);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    FinanceReport(FinanceReport F,int size)
    {
        array=new Payment[size];
        this.NameOfCompiler=F.getNameOfCompiler();
        this.DayOfCreation=F.getDayOfCreation();
        this.MonthOfCreation=F.getMonthOfCreation();
        this.YearOfCreation=F.getYearOfCreation();
        for(int i=0;i<size;i++)
        {
            array[i]=new Payment();
        }
    }

    public Payment[] getArray() {
        return array;
    }

    public String getNameOfCompiler() {
        return NameOfCompiler;
    }

    public int getDayOfCreation() {
        return DayOfCreation;
    }

    public int getMonthOfCreation() {
        return MonthOfCreation;
    }

    public int getYearOfCreation() {
        return YearOfCreation;
    }



    FinanceReport(FinanceReport obj) {
      array = new Payment[obj.array.length];

        for(int i=0;i<array.length;i++)
        {
            array[i] = new Payment();
            array[i].PaymentClone(obj.array[i]);
        }
     this.NameOfCompiler=obj.NameOfCompiler;
     this.DayOfCreation=obj.DayOfCreation;
     this.MonthOfCreation=obj.MonthOfCreation;
     this.YearOfCreation=obj.YearOfCreation;
    }

    public void FinanceReportAdd(int index) throws IOException {
        BufferedReader BF =new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter day of payment:\n");
            String Sday = BF.readLine();
            int day=Integer.parseInt(Sday);
        System.out.println("Enter month of payment:\n");
        String Smonth = BF.readLine();
        int month = Integer.parseInt(Smonth);
        System.out.println("Enter year of payment:\n");
        String Syear=BF.readLine();
        int year = Integer.parseInt(Syear);
        System.out.print("Enter name of payer:\n");
        String name = BF.readLine();
        System.out.println("Enter sum of payment:\n");
        String Ssum=BF.readLine();
        int sum = Integer.parseInt(Ssum);
        array[index]=new Payment(day,month,year,sum,name);
    }

    public int CountOfPayments() {
        return array.length;
    }

    public void PaymentAccess(int index) {
        System.out.println("Choose:\n1.Read\n2.Write\n");
        Scanner in = new Scanner(System.in);
        int choice = in.nextInt();
        if (choice == 1) {
            array[index].PaymentToString();
        }
        if (choice == 2) {
            try {
                FinanceReportAdd(index);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public void FinanceReportToString() {
        String sf;
        sf = String.format("[Автор: %s, Дата:%d.%d.%d\nПлатежи:\n[\n", this.NameOfCompiler, DayOfCreation, MonthOfCreation, YearOfCreation);
        System.out.println(sf);
        for (int i = 0; i < array.length; i++) {
        System.out.println(String.format("Плательщик: %s , Дата: %d.%d.%d , Сумма: %d руб, %02d коп.\n", array[i].getName(), array[i].getDayPay(), array[i].getMonthPay(), array[i].getYearPay(), array[i].getSumPay() / 100, array[i].getSumPay() % 100));
        }
        System.out.println("]");
    }

}
