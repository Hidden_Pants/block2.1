package com.company;

import javax.swing.*;

public class FinanceReportProcessor {
    private static FinanceReport finance;

    public FinanceReportProcessor(FinanceReport finance) {
        this.finance = finance;
    }

    public static FinanceReport ReturnSecondNamePayers(char c) {
        System.out.println("ReturnSecondNamePayers:\n");
        int size = 0;
        for (int i = 0; i < finance.getArray().length; i++) {
            if (finance.getArray()[i].getName().charAt(0) == c) {
                size++;
            }
        }
        FinanceReport f1 = new FinanceReport(finance, size);
        int j = 0;
        for (int i = 0; i < f1.getArray().length; i++) {
            for (int temp = j; temp < finance.getArray().length; temp++) {
                if (finance.getArray()[j].getName().charAt(0) == c) {
                    f1.getArray()[i].PaymentClone(finance.getArray()[j]);
                    j++;
                    break;
                } else {
                    j++;
                }
            }

        }
        return f1;
    }

    public static FinanceReport AllLowerPayments(int value) {
        int size=0;
        System.out.println("AllLowerPayments:\n");
        for (int i = 0; i < finance.getArray().length; i++) {
            if (finance.getArray()[i].getSumPay() < value) {
                size++;
            }
        }
        FinanceReport f1= new FinanceReport(finance,size);
        int j=0;
        for(int i=0; i < f1.getArray().length;i++) {
            for (int temp = j; j < finance.getArray().length; temp++) {
                if (finance.getArray()[j].getSumPay() < value) {
                    f1.getArray()[i].PaymentClone(finance.getArray()[j]);
                    j++;
                    break;
                }
                else
                {
                    j++;
                }
            }

        }
        return f1;
    }

    /*public static FinanceReport SummarySumOfPayments(String str) {
        char[] str1 = new char[2];
        str.getChars(0, 2, str1, 0);
        int day = Integer.parseInt(String.valueOf(str1));
        str.getChars(3, 5, str1, 0);
        int month = Integer.parseInt(String.valueOf(str1));
        str1 =new char[4];
        str.getChars(6, 10, str1, 0);
        int year = Integer.parseInt(String.valueOf(str1));
        int TotalSum = 0;
        for (int i = 0; i < finance.getArray().length; i++) {
            if (finance.getArray()[i].getDayPay() == day && finance.getArray()[i].getMonthPay() == month && finance.getArray()[i].getYearPay() == year) {
                TotalSum += finance.getArray()[i].getSumPay();
            }
        }
        System.out.println("TotalSum: "+TotalSum/100 + "руб., " + TotalSum%100 + " коп." );
        return finance;
    }
    public static FinanceReport ListOfMonths(int year) {
        String months[] = {"January", "February", "March", "April", "May", "june", "July", "August", "September", "October", "November", "December"};
        boolean flag[] = new boolean[12];
        int count=0;
        for (int i=0;i<12;i++)
        {
            flag[i]=false;
        }
        System.out.println("Months:");
        for (int i = 0; i < finance.getArray().length; i++) {
            if (finance.getArray()[i].getYearPay() == year) {
                flag[finance.getArray()[i].getMonthPay()-1]=true;
            }
        }
        for(int i=0;i<flag.length;i++) {
            if (flag[i] == true) {
                System.out.println(months[i]);
                count++;
            }
        }
        if(count==0)
        {
            System.out.println("No information\n");
        }
        return finance;
    }
*/
}
