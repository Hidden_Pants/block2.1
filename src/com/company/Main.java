package com.company;

public class Main {

    public static void main(String[] args)
    {
        Payment q1 = new Payment(13,9,2007,1300000,"Зубенко Михаил Петрович");
        System.out.println(q1.PaymentToString());
        FinanceReport rep1 = new FinanceReport(21,10,1999,"Зубенко Михаил Петрович",3);
        FinanceReport rep2 = new FinanceReport(rep1);
        rep1.FinanceReportToString();
        FinanceReportProcessor processor =new FinanceReportProcessor(rep1);
        FinanceReport rep3 =processor.ReturnSecondNamePayers('З');
        rep3.FinanceReportToString();
        rep3=processor.AllLowerPayments(100000);
        rep3.FinanceReportToString();
    }
}
