package com.company;

import java.util.Objects;

public class Payment {
    private String name;
    private int DayPay, MonthPay, YearPay,SumPay;

    Payment() {
        name = "";
    }

    Payment(int DayPay, int MonthPay, int YearPay, int SumPay, String name) {
        this.name = name;
        this.DayPay = DayPay;
        this.MonthPay = MonthPay;
        this.YearPay = YearPay;
        this.SumPay = SumPay;
    }

    public void PaymentChanger(int DayPay, int MonthPay, int YearPay, int SumPay, String name) {
        this.name = name;
        this.DayPay = DayPay;
        this.MonthPay = MonthPay;
        this.YearPay = YearPay;
        this.SumPay = SumPay;
    }
    public void PaymentClone(Payment obj)
    {
        this.name = obj.getName();
        this.DayPay = obj.getDayPay();
        this.MonthPay = obj.getMonthPay();
        this.YearPay = obj.getYearPay();
        this.SumPay = obj.getSumPay();
    }

    public int getDayPay() {
        return DayPay;
    }

    public int getMonthPay() {
        return MonthPay;
    }

    public int getYearPay() {
        return YearPay;
    }

    public int getSumPay() {
        return SumPay;
    }

    public void setDayPay(int dayPay) {
        DayPay = dayPay;
    }

    public void setMonthPay(int monthPay) {
        MonthPay = monthPay;
    }

    public void setYearPay(int yearPay) {
        YearPay = yearPay;
    }

    public void setSumPay(int sumPay) {
        SumPay = sumPay;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Payment[] ArrayCopy(Payment[] obj)
    {
        Payment[] arr = new Payment[obj.length];
        for(int i=0;i<arr.length;i++)
        {
            arr[i].setDayPay(obj[i].getDayPay());
            arr[i].setMonthPay(obj[i].getMonthPay());
            arr[i].setYearPay(obj[i].getYearPay());
            arr[i].setSumPay(obj[i].getSumPay());
            arr[i].setName(obj[i].getName());
        }
        return arr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Payment payment = (Payment) o;
        return DayPay == payment.DayPay &&
                MonthPay == payment.MonthPay &&
                YearPay == payment.YearPay &&
                SumPay == payment.SumPay &&
                Objects.equals(name, payment.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, DayPay, MonthPay, YearPay, SumPay);
    }

    @Override
    public String toString() {
        return "Payment{" +
                "name='" + name + '\'' +
                ", DayPay=" + DayPay +
                ", MonthPay=" + MonthPay +
                ", YearPay=" + YearPay +
                ", SumPay=" + SumPay +
                '}';
    }

    public String PaymentToString() {
        return String.format("Плательщик: %s , Дата: %d.%d.%d , Сумма: %d руб, %02d коп.\n", getName(), getDayPay(), getMonthPay(), getYearPay(), getSumPay()/100,getSumPay()%100);
                //"Дата: " + this.DayPay + "." + this.MonthPay + "." + this.YearPay + "; Имя: " + this.name + "; Сумма платежа: " + this.SumPay/100 + " руб.," + " "+this.SumPay%100 + "коп.";
    }
}
